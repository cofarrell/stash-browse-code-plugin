# Stash Browse Code Plugin

A simple plugin for adding a "Browse Code" link on the commits page.

![screenshot](raw/master/screenshot.png)
